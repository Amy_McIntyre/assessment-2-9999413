﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Popups;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Assessment2_9999413
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public string to = "";
        public string from = "";
        public int priority = 0;
        public string msgbox = "";

        public MainPage()
        {
            this.InitializeComponent();
        }

        private async void submitbutton_Click(object sender, RoutedEventArgs e)
        {
            string to = tobox.Text;
            string from = frombox.Text;
            int priority = subjectbox.SelectedIndex;
            string msgbox = messagebox.Text;

            string x = subPriority(priority);

            MessageDialog outputbox = new MessageDialog($"\nTo: {to} \nFrom: {from} \nSubject Priority: {x} \n\tMessage: {msgbox} ");
            outputbox.Title = "Are you happy with your message?";

            outputbox.Commands.Add(new Windows.UI.Popups.UICommand($"Send to {to}") { Id = 0 });
            outputbox.Commands.Add(new Windows.UI.Popups.UICommand("Cancel") { Id = 1 });
            outputbox.Commands.Add(new Windows.UI.Popups.UICommand("Reset") { Id = 2 });

            var result = await outputbox.ShowAsync();

            if ((int)result.Id == 0)
            {
                Application.Current.Exit();
            }
            else if ((int)result.Id == 2)
            {
                tobox.Text = "";
                frombox.Text = "";
                subjectbox.SelectedIndex = -1;
                messagebox.Text = "";
            }


        }
        static string subPriority(int priority)
        {
            var z = "";

            switch (priority)
            {
                case 0:
                    z = "Low Priority";
                    break;

                case 1:
                    z = "Medium Priority";
                    break;

                case 2:
                    z = "High Priority";
                    break;

                case 3:
                    z = "Urgent Priority";
                    break;

            }
            return z;
        }
    }
}
